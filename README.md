# Dev Camp Project Files

These files are for defining the machines and projects used in the [Dev Manager web application](https://gitlab.com/devmanager/devmanager). You can 
make symbolic links to the files from the target files in this repo to their links in the Dev Manager working directory:

## Windows
	mklink \path\to\devmanager\config\remote.php \path\to\dc-projects\config\remote.php
## Linux/OS X	
	ln -s /path/to/dc-projects/config/remote.php /path/to/devmanager/config/remote.php
	
This will allow you to edit the files in a separate directory that tracks the files for this repo while they are in use in
the main Dev Manager project.