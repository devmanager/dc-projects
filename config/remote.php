<?php
	return [
		/*
		|--------------------------------------------------------------------------
		| Default Remote Connection Name
		|--------------------------------------------------------------------------
		|
		| Here you may specify the default connection that will be used for SSH
		| operations. This name should correspond to a connection name below
		| in the server list. Each connection will be manually accessible.
		|
		*/
		'default'     => 'development',
		/*
		|--------------------------------------------------------------------------
		| Remote Server Connections
		|--------------------------------------------------------------------------
		|
		| These are the servers that will be accessible via the SSH task runner
		| facilities of Laravel. This feature radically simplifies executing
		| tasks on your servers, such as deploying out these applications.
		|
		*/
		'connections' => [
			'production' => [
				'host'      => '',
				'username'  => '',
				'password'  => '',
				'key'       => '',
				'keyphrase' => '',
				'root'      => '/var/www',
				'setup'		=> ''
			],
			'development' => [
					/* Replace this with the remote computer's hostname */
					'host'      => 'localhost', 
					'username'  => 'setupadmin',
					/* Replace this with the absolute path of the private key that matches the public key
					 * that is appended to setupadmin's authorized_keys file */
					'key'       => env('SSH_KEY'),
					'keyphrase' => '',
					'root'      => '/var/www',
					'setup'		=> 'php -f ~/devmanager/testsetup.php',
					'projects' => [
						[
							'name' => 'Dev Camp',
							'authtype' => 'password',
							'canChooseGroup' => true,
							'getGroups' => 'php -f ~/devmanager/getGroups.php',
							'defaultGroup' => ["label" => "Beaver Alpha", "name" => "dc-team1"],
							'managingGroup' => 'developers',
							'canChooseShell' => false,
							'getShells' => 'php -f ~/devmanager/getShells.php',
							'defaultShell' => '/bin/false',
							'setup' => 'php -f ~/devmanager/testprojectsetup.php',
							'addUser' => 'php -f ~/devmanager/addUser.php',
							'getStatus' => 'php -f ~/devmanager/getStatus.php'
						]
					]
			]
		],
		/*
		|--------------------------------------------------------------------------
		| Remote Server Groups
		|--------------------------------------------------------------------------
		|
		| Here you may list connections under a single group name, which allows
		| you to easily access all of the servers at once using a short name
		| that is extremely easy to remember, such as "web" or "database".
		|
		*/
		'groups'      => [
			'web' => [ 'production' ]
		],
	];